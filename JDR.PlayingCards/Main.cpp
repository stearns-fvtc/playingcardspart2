
// Playing Cards
// Justin Rankin
// Tyler Stearns

#include <iostream>
#include <conio.h>

using namespace std;

string cardName[13]
{
	"Two",
	"Three",
	"Four",
	"Five",
	"Six",
	"Seven",
	"Eight",
	"Nine",
	"Ten",
	"Jack",
	"Queen",
	"King",
	"Ace"
};

string cardSuit[4]
{
	"Hearts",
	"Diamonds",
	"Spades",
	"Clubs"
};


enum class Suit
{
	Hearts,
	Diamonds,
	Spades,
	Clubs
};

enum class Rank 
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	cout << "The " << cardName[(int)card.Rank - 2] << " of " << cardSuit[(int)card.Suit];
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank) return card1;
	else return card2;
}

int main()
{
	// test code for debugging
	Card c1;
	c1.Rank = Rank::King;
	c1.Suit = Suit::Spades;

	Card c2;
	c2.Rank = Rank::Jack; 
	c2.Suit = Suit::Clubs;

	PrintCard(c1);
	cout << "\n";
	PrintCard(c2);
	cout << "\n";

	PrintCard(HighCard(c1, c2));
	cout << " is the high card.";

	(void)_getch();
	return 0;
}
